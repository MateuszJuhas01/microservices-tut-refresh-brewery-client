package com.example.breveryclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BreveryClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(BreveryClientApplication.class, args);
	}

}
